# Proton wire

A Python tool to locate continuous proton wires composed of h-bonded
donor-acceptor pairs that can instantaneously transmit protons via
a membrane channel using a Grotthuss-like mechanism

## Getting Started

To run the script, you need (1) a trajectory to analyze (in any
format recognized by mdtraj.load) and (2) a PDB file compatible
with the trajectory. Numpy and MDTraj should be installed;
if not available, both can be obtained using pip:

```
pip install numpy mdtraj
```

Run "python proton\_wire.py -h" to see available options.

## Usage

In the simplest version, you need to supply the trajectory,
the structure and a (MDTraj-compatible) query/selection that will
define the channel-forming moiety:

```
python proton_wire.py -f trajectory.xtc -s firstframe.pdb -q protein
python proton_wire.py -f trajectory.xtc -s firstframe.pdb -q "resname AMB"
```

Optionally, you can specify:

+ the output file with `-o` (default wires.dat)
+ the distance criterion used to define the water layer around
channel atoms with `-l` (only water molecules whose oxygen atoms are no
 further than this distance from any channel atom will be considered
 when looking for wires; default 0.45 nm)
+ the angle criterion for h-bond with `-a` (minimum acceptor-hydrogen-donor
angle for an h-bond; default 120 degrees)
+ first distance criterion for h-bond with `-d` (maximum acceptor-donor
distance for an h-bond; default 0.35 nm)
+ second distance criterion for h-bond with `-b` (maximum hydrogen-acceptor
distance for an h-bond; default 0.25 nm)
+ the maximum "backwards rise" along the wire with `-r` (in general,
consicutive atoms in the wire should go down in the Z-direction; this
option allows to override this behavior and allow atoms to "go back"
in the Z-dir; default 0.0 nm)
+ wire orientation with respect to the Z-axis with `-m` ("u" for up, "d" for down,
'ud' for either and 'n' for none; e.g. "u" means that all h-bond donors
point upwards with their hydrogen atoms, i.e. proton transfer occurs
from the bottom to the top)

  General usage (parameters in square brackets are optional):

```
python proton_wire.py -f trajectory.xtc -s firstframe.pdb -q channel_query [-o output_file -l water_layer_thickness -a angle_criterion_hb -d distance_criterion_hb -b hb_length_criterion -r max_backwards_rise -m orientational_mode]
```
