import numpy as np
import mdtraj as md
import time
import sys
from optparse import OptionParser


class Data:
    def __init__(self, water_threshold, pdb, traj, channel_selector, phos_selector, angle,
                 ha, outfile, max_rise, d, mode, memlen):
        self.threshold = water_threshold
        self.current_box = None
        self.outfile = open(outfile, 'w')
        self.traj = md.iterload(traj, top=pdb, chunk=10)
        self.data = md.load(pdb)
        self.selector = channel_selector
        self.max_rise = max_rise
        self.angle_criterion = angle
        self.ha_criterion = ha
        self.distance_criterion = d
        self.mode = mode
        self.memory_length = memlen
        self.memory = {}
        self.water_oxygens = self.data.top.select("resname TIP3 SOL HOH and symbol O")
        self.ps = self.data.top.select(phos_selector)
        self.wires = None
        self.longest_wire = None
    
    def analyze_frame(self, frame):
        self.current_box = frame.unitcell_lengths[0]
        self.wires = []
        self.longest_wire = []
        t = Timer()
        channel_on = self.data.top.select("{} and symbol O N".format(self.selector))
        ch_hydrogen_indices = self.data.top.select("{} and symbol H".format(self.selector))
        channel_all = self.data.top.select("{}".format(self.selector))
        if len(channel_all) == 0:
            print("Warning: selection {} returns no atoms".format(self.selector))
            sys.exit(1)
        lower, upper = self.find_bounds(frame)
        t("setting up groups")
        # get water oxygens
        waters = md.compute_neighbors(frame,
                                      cutoff=self.threshold,
                                      query_indices=channel_all,
                                      haystack_indices=self.water_oxygens)[0]
        t("finding water atoms")
        # get only water-bound channel oxygens/nitrogens
        chans = md.compute_neighbors(frame,
                                     cutoff=self.threshold,
                                     query_indices=waters,
                                     haystack_indices=channel_on)[0]
        t("finding channel atoms")
        wire_accs = [PolarHeavy(frame.xyz[0, x, :], frame.xyz[0, x + 1:x + 3, :], x) for x in waters]
        for ch_acc in chans:
            hydrogens = md.compute_neighbors(frame,
                                             cutoff=0.115,
                                             query_indices=[ch_acc],
                                             haystack_indices=ch_hydrogen_indices)[0]
            wire_accs.append(PolarHeavy(frame.xyz[0, ch_acc, :], hydrogens, ch_acc))
        t("collecting acceptors")
        coords_acc = frame.xyz[0, np.concatenate((waters, chans)).astype(np.int32), :]
        for i in wire_accs:
            i.get_neighbors(wire_accs, coords_acc, self.distance_criterion)
        t("finding neighbors")
        # source refers to group of upper acc atoms
        source = [x for x in wire_accs if self.vec_pbc(np.array([0, 0, upper]), x.xyz)[2] > 0 >
                  self.vec_pbc(np.array([0, 0, upper+2]), x.xyz)[2]]
        # source refers to group of bottom acc atoms
        target = [x for x in wire_accs if self.vec_pbc(np.array([0, 0, lower - 2]), x.xyz)[2] > 0 >
                  self.vec_pbc(np.array([0, 0, lower]), x.xyz)[2]]
        if len(source) * len(target) == 0:
            print("No source or target water molecules found; did you set your channel & phosphate groups correctly?")
        if 'd' in self.mode:
            for x in source + list(self.memory.keys()):
                # look for wires that go from source to target in the h-bond direction (D-H--A):
                self.check_wire(x, target, [], 'down')
        if 'u' in self.mode:
            target, source = source, target
            for x in source + list(self.memory.keys()):
                # look for wires that go from source to target in the opposite direction (A--H-D):
                self.check_wire(x, target, [], 'up')
        if 'n' in self.mode:
            for x in source + list(self.memory.keys()):
                # no strict h-bonding direction enforced:
                self.check_wire(x, target, [], 'any')
        if any([x not in 'dun' for x in self.mode]):
            raise ValueError("Mode field can only contain letters 'd', 'u' or 'n'")
        t("locating wires")
        if self.wires:
            shortest_wire = self.wires[int(np.argmin([len(x) for x in self.wires if x]))]
            if shortest_wire[0] in self.memory.keys():
                first = shortest_wire[0]
                self.outfile.write("time={} {}\n".format(
                    frame.time[0], " ".join([str(x) for x in self.memory[first][1] + shortest_wire[1:]])))
            else:
                self.outfile.write("time={} {}\n".format(frame.time[0], " ".join([str(x) for x in shortest_wire])))
        else:
            self.outfile.write("time={} No wires found, "
                               "longest partial wire: {}\n".format(frame.time[0],
                                                                   " ".join([str(x) for x in self.longest_wire])))
            if len(self.longest_wire) > 0:
                self.memory[self.longest_wire[-1]] = [self.memory_length, self.longest_wire[:]]
        self.forget()
        self.outfile.flush()
    
    def forget(self):
        for i in list(self.memory.keys()):
            if self.memory[i][0] == 0:
                self.memory.pop(i)
        for i in list(self.memory.keys()):
            self.memory[i][0] -= 1
    
    def check_wire(self, heavy, target, visited, mode="any"):
        for next_heavy in heavy.neighbors:
            if mode == "up":
                donor = next_heavy
                acceptor = heavy
                hydrogens = donor.hydrogens
            elif mode == "down":
                donor = heavy
                acceptor = next_heavy
                hydrogens = donor.hydrogens
            elif mode == "any":
                donor = next_heavy
                acceptor = heavy
                hydrogens = donor.hydrogens + acceptor.hydrogens
            else:
                return ValueError("mode has to be up, down or any")
            # if next_heavy not in visited and vertical rise lower than threshold:
            if next_heavy not in visited and self.vec_pbc(heavy.xyz, next_heavy.xyz)[2] < self.max_rise:
                for h in hydrogens:
                    if self.check_hbond(donor.xyz, h, acceptor.xyz, check_angle=True, check_hydrogen_acceptor=True):
                        visited.append(next_heavy)
                        if next_heavy in target:
                            self.wires.append(visited)
                        else:
                            self.check_wire(next_heavy, target, visited, mode)
                    else:
                        if len(visited) > len(self.longest_wire):
                            self.longest_wire = visited
            else:
                if len(visited) > len(self.longest_wire):
                    self.longest_wire = visited

    def find_bounds(self, frame):
        p = []
        p1 = []
        p2 = []
        ref = frame.xyz[0, self.ps[0]]
        for i in self.ps:
            p.append(self.vec_pbc(ref, frame.xyz[0, i]))
        for at in p:
            if np.abs(at[2] - p[0][2]) < 1.5:
                p1.append(ref[2] + at[2])
            else:
                p2.append(ref[2] + at[2])
        if np.mean(p1) > np.mean(p2):
            upper, lower = np.mean(p1), np.mean(p2)
        else:
            upper, lower = np.mean(p2), np.mean(p1)
        print(lower, upper)
        return lower, upper
    
    def check_hbond(self, xyz_d, xyz_h, xyz_a, check_angle=True, check_hydrogen_acceptor=True):
        d_dh = self.pbc_dist(xyz_d, xyz_h)
        d_ha = self.pbc_dist(xyz_h, xyz_a)
        if check_angle:
            angle = np.arccos(np.dot(self.vec_pbc(xyz_h, xyz_a), self.vec_pbc(xyz_h, xyz_d)) / (d_dh * d_ha))
            if angle*180/np.pi < self.angle_criterion:
                return False
        if check_hydrogen_acceptor:
            if d_ha > self.ha_criterion:
                return False
        return True

    def vec_pbc(self, xyz1, xyz2):
        boxsize = self.current_box
        dr = xyz2 - xyz1
        return np.mod(dr+boxsize/2, boxsize) - boxsize/2
    
    def pbc_dist(self, xyz1, xyz2):
        return np.linalg.norm(self.vec_pbc(xyz1, xyz2))


class Timer:
    def __init__(self):
        self.time = time.time()
        
    def __call__(self, msg="time from previous report"):
        pass
        # only for debugging purposes
        # print("{}: {}\n".format(msg, time.time() - self.time))
        # self.time = time.time()


class PolarHeavy:
    def __init__(self, xyz, hydrogens_xyz, name):
        self.name = name
        self.xyz = xyz
        self.hydrogens = [h for h in hydrogens_xyz]
        self.neighbors = None
        
    def get_neighbors(self, other_accs, xyzs, distance):
        neighs = np.where(np.sum((xyzs-self.xyz)**2, 1)**0.5 < distance)[0]
        self.neighbors = [other_accs[x] for x in neighs if other_accs[x] is not self]
        
    def __repr__(self):
        return str(self.name)
    
    def __str__(self):
        return str(self.name)


def dist(xyz1, xyz2):
    return np.linalg.norm(xyz1-xyz2)


def main():
    opts = parse_all()
    d = Data(water_threshold=opts.layer,
             pdb=opts.pdb,
             traj=opts.traj,
             channel_selector=opts.sel,
             phos_selector=opts.psel,
             outfile=opts.out,
             angle=opts.angle,
             ha=opts.dist_ha,
             d=opts.dist_da,
             max_rise=opts.rise,
             mode=opts.mode,
             memlen=opts.memlen)
    for chunk in d.traj:
        for frame in chunk:
            print("analyze frame for time {}".format(frame.time[0]))
            d.analyze_frame(frame)
    d.outfile.close()


def parse_all():
    parser = OptionParser(usage="")
    parser.add_option("-s", dest="pdb", action="store", type="string",
                      help="PDB file")
    parser.add_option("-f", dest="traj", action="store", type="string",
                      help="Trajectory file")
    parser.add_option("-q", dest="sel", action="store", type="string",
                      help="Selection (MDTraj-compatible) used to select channel atoms, "
                           "e.g. 'resname AMFB' or 'protein'")
    parser.add_option("-p", dest="psel", action="store", type="string", default="element P",
                      help="Selection (MDTraj-compatible) used to select phosphorus (or other membrane surface) atoms, "
                           "e.g. 'element P and resid < 200'; default is 'element P'")
    parser.add_option("-o", dest="out", action="store", type="string", default="wires.dat",
                      help="Output file")
    parser.add_option("-l", dest="layer", action="store", type="float", default=0.45,
                      help="Distance criterion for the water layer (only water oxygens within this distance"
                           "from channel atoms are included in the calculation); default is 0.45 nm")
    parser.add_option("-a", dest="angle", action="store", type="float", default=120,
                      help="Angle criterion for h-bonds (minimum D-H-A angle required for "
                           "the interaction to be classified as an h-bond); default is 120 degrees")
    parser.add_option("-d", dest="dist_da", action="store", type="float", default=0.35,
                      help="Distance criterion for h-bonds (maximum D-A distance required for "
                           "the interaction to be classified as an h-bond); default is 0.35 nm")
    parser.add_option("-b", dest="dist_ha", action="store", type="float", default=0.25,
                      help="Second distance criterion for h-bonds (maximum H-A distance required for "
                           "the interaction to be classified as an h-bond); default is 0.25")
    parser.add_option("-r", dest="rise", action="store", type="float", default=0.0,
                      help="Max rise along the z-direction between two consecutive h-bond acceptors in the wire; "
                           "default is 0, so the Z-position of wire atoms has to change monotonously")
    parser.add_option("-m", dest="mode", action="store", type="string", default="ud",
                      help="Enforce the h-bond pattern strictly going up (u), down (d), either up or down (ud) "
                           "or do not require uniform orientation (n); default is ud")
    parser.add_option("-k", dest="memlen", action="store", type="int", default=0,
                      help="Memory length, i.e., for how many frames the ends of the longest wires "
                           "in K previous frames can seed a wire in the current frame; default is 0 "
                           "(reasonable if stride is large; makes sense to restrict history to ~100 ps)")
    (options, args) = parser.parse_args()
    return options


if __name__ == "__main__":
    main()
